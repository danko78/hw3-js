let firstNumber = +prompt("Enter the first number");
while (isNaN(firstNumber)) {
    firstNumber = +prompt("Please enter the correct number");
}
let secondNumber = +prompt("Enter the second number");
while (isNaN(secondNumber)) {
    secondNumber = +prompt("Please enter the correct number");
}
let operation = prompt("Enter math operation:  +, -, *, /.");
while (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/") {
    operation = prompt("Enter the correct operation");
}

function mathOperation(firstNum, secondNum, operationMath) {
    switch (operationMath) {
        case "+":
            return firstNum + secondNum;
        case "-":
            return firstNum - secondNum;
        case "*":
            return firstNum * secondNum;
        case "/":
            return firstNum / secondNum;
    }
}
console.log(mathOperation(firstNumber, secondNumber, operation));